# LeetCodePractice
This project is a simple console application that runs my own LeetCode unit tests against my solutions. This 
project is purely for algorithm practice and to show potential employers that I actually know how to write 
code and test it. (emphasis on the test it part lol) If you are reading this, thank you for taking the time to 
look at my code!
