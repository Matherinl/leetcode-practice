﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LeetCodePractice {
	///<summary>This class holds all solutions to LeetCode problems. Regions are specified for the type of problem. Each problem has a corresponding unit test class.
	///The summary of each method will be the LeetCode problem text. This class is static to make unit testing simple.</summary>
	public static class LeetCodeSolutions {
		#region Easy Problems
		///<summary>#7-Given a 32-bit signed integer, reverse digits of an integer.
		///Example - Input:123, Output:321
		///Input:-123, Output:-321
		///Input:21, Output:21</summary>
		public static int Reverse(int x) {
			char[] arrayChars=x.ToString().ToCharArray();
			bool isNegative=false;
			string reversedNum="";
			for(int i=arrayChars.Length-1;i>=0;i--) {
				if(arrayChars[i]=='-') {//Skip '-' character and flag the number as negative
					isNegative=true;
					continue;
				}
				if(reversedNum=="" && arrayChars[i]=='0') {//Only trim off leading 0's
					continue;
				}
				reversedNum+=arrayChars[i];
			}
			int retVal;
			try {
				retVal=Int32.Parse(reversedNum);
			}
			catch(OverflowException ex) {//Any overflow exceptions should return 0 per the instructions
				return 0;
			}
			catch(FormatException ex) {
				return 0;
			}
			return isNegative ? retVal*-1 : retVal;
		}
		
		///<summary>#7 - An improved approach using C#'s built in string character index. By avoiding
		///a char[] we can improve the speed and memory usage of the Reverse() method. This improvement
		///changed my solution from being better than 23.69% of submissions to better than 87.04% submissions.</summary>
		public static int ReverseEnhanced(int x) {
			string num=x.ToString();
			if(num.Length==1) {
				return x;
			}
			string reversedNum="";
			bool isNegative=false;
			for(int i=num.Length-1;i>=0;i--) {
				if(num[i]=='-') {
					isNegative=true;
					continue;
				}
				if(reversedNum=="" &&num[i]=='0') {
					continue;
				}
				reversedNum+=num[i];
			}
			int retVal;
			try {
				retVal=Int32.Parse(reversedNum);
			}
			catch(OverflowException ex) {
				return 0;
			}
			catch(FormatException ex) {
				return 0;
			}
			return isNegative ? retVal*-1 : retVal;
		}

		///<summary>#9 - Determine whether an integer is a palindrome. An integer is a palindrome 
		///when it reads the same backward as forward. This implementation is considerably slow compared
		///to other solutions. (Only faster than 5.98% submissions). See enhanced approach below.</summary>
		public static bool IsPalindrome(int x) {
			if(x<0) {//Per the problem definition negatives cannot be palindromes.
				return false;
			}
			if(ReverseEnhanced(x)==x) {
				return true;
			}
			return false;
		}

		///<summary>#9 - An attempt to improve the algorithm of IsPalindrome(). This time we will do it
		///by comparing the integer values of each digit place against their opposite. We could do this using
		///a char[]. It is definitely faster to do this using math and avoiding a char[]. However, the it is 
		///much simpler and clear using a char[]. This approach was 20ms faster on LeetCode but still only
		///faster than 5.98% of entries. Without a doubt because we use a char[] instead of raw math.</summary>
		public static bool IsPalindromeEnhanced(int x) {
			string strInteger=x.ToString();
			int i=0;//Index pointer to the start of the array, walks forwards
			int k=strInteger.Length-1;//Index pointer to end of array, walks backwards
			while(i<=k) {
				if(strInteger[i]!=strInteger[k]) {
					return false;
				}
				i++;
				k--;
			}
			return true;
		}

		///<summary>Roman numerals are represented by seven different symbols: I, V, X, L, C, D and M.
		///I can be placed before V (5) and X (10) to make 4 and 9.
		///X can be placed before L (50) and C (100) to make 40 and 90.
		///C can be placed before D (500) and M (1000) to make 400 and 900.
		///Given a roman numeral, convert it to an integer. Input is guaranteed to be within the range from 1 to 3999.</summary>
		public static int RomanToInt(string s) {
			int retVal=0;//return value, added to as we break down the roman numeral string
			int i=0;//pointer to single roman numeral character in s
			string numeral;
			string nextNumeral="";
			int numeralLength=s.Length;
			while(i<numeralLength) {
				numeral=s[i].ToString();
				if(i+1<numeralLength) {
					nextNumeral=s[i+1].ToString();
				}
				//Check for Roman Numeral subtraction. Per the problem description we only have to worry about I,X,C.
				if(numeral=="I") {
					if(nextNumeral=="V" || nextNumeral=="X") {
						retVal+=RomanToIntHelper(numeral+nextNumeral);
						i+=2;//Skip 2 places because we evaluated the next character already.
					}
					else {
						retVal+=RomanToIntHelper(numeral);
						i++;
					}
				}
				else if(numeral=="X") {
					if(nextNumeral=="L" || nextNumeral=="C") {
						retVal+=RomanToIntHelper(numeral+nextNumeral);
						i+=2;//Skip 2 places because we evaluated the next character already.
					}
					else {
						retVal+=RomanToIntHelper(numeral);
						i++;
					}
				}
				else if(numeral=="C") {
					if(nextNumeral=="D" || nextNumeral=="M") {
						retVal+=RomanToIntHelper(numeral+nextNumeral);
						i+=2;//Skip 2 places because we evaluated the next character already.
					}
					else {
						retVal+=RomanToIntHelper(numeral);
						i++;
					}
				}
				else {//Not a candidate for Roman Numeral subtraction.
					retVal+=RomanToIntHelper(numeral);
					i++;
				}
			}
			return retVal;
		}

		///<summary>This approach is similar in speed but I consider it 'improved' because it is much much simpler.</summary>
		public static int RomanToIntImproved(string s) {
			int retVal=0;
			foreach(char c in s) {
				retVal+=RomanToIntHelper(c.ToString());
			}
			string numeral="";
			string nextNumeral="";
			for(int i=0;i<s.Length;i++) {
				numeral=s[i].ToString();
				if(i+1<s.Length) {
					nextNumeral=s[i+1].ToString();
				}
				else {
					nextNumeral="";
				}
				if(numeral=="I" && nextNumeral=="V") {
					retVal-=2;
				}
				else if(numeral=="I" && nextNumeral=="X") {
					retVal-=2;
				}
				else if(numeral=="X" &&nextNumeral=="L") {
					retVal-=20;
				}
				else if(numeral=="X" &&nextNumeral=="C") {
					retVal-=20;
				}
				else if(numeral=="C" &&nextNumeral=="D") {
					retVal-=200;
				}
				else if(numeral=="C" &&nextNumeral=="M") {
					retVal-=200;
				}
			}
			return retVal;
		}

		///<summary>Case statement that handles Roman Numeral to integer as well as Roman Numeral subtraction.</summary>
		private static int RomanToIntHelper(string s) {
			int retVal=0;
			switch(s) {
				case "I":
					retVal=1;
					break;
				case "IV":
					retVal=4;
					break;
				case "V":
					retVal=5;
					break;
				case "IX":
					retVal=9;
					break;
				case "X":
					retVal=10;
					break;
				case "XL":
					retVal=40;
					break;
				case "L":
					retVal=50;
					break;
				case "XC":
					retVal=90;
					break;
				case "C":
					retVal=100;
					break;
				case "CD":
					retVal=400;
					break;
				case "D":
					retVal=500;
					break;
				case "CM":
					retVal=900;
					break;
				case "M":
					retVal=1000;
					break;
			}
			return retVal;
		}

		///<summary>Write a function to find the longest common prefix string amongst an array of strings.
		///If there is no common prefix, return an empty string "".</summary>
		public static string LongestCommonPrefix(string[] strs) {
			if(strs.Length==0) {
				return "";
			}
			else if(strs.Length==1) {
				return strs[0];
			}
			//Figure out how many iterations we will need to make, at most.
			int smallestWord=int.MaxValue;
			foreach(string str in strs) {
				if(str.Length<smallestWord) {
					smallestWord=str.Length;
				}
			}
			StringBuilder prefix=new StringBuilder();
			string firstWord=strs[0];
			bool exitLoop=false;//Signals when we are done.
			//Use the first word for comparison to the others.
			for(int i = 0;i<smallestWord;i++) {//Outer loop
				for(int j = 1;j<strs.Length;j++) {//Loop through each of the words
					if(strs[j][i]!=firstWord[i]) {
						exitLoop=true;
						break;
					}
					else if(j==strs.Length-1) {//We are on the last word, so they must all match character i from firstWord
						prefix.Append(firstWord[i]);
					}
				}
				if(exitLoop) {
					break;
				}
			}
			return prefix.ToString();
		}

		///<summary>Given a string containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid. 
		///An input string is valid if:
		///Open brackets must be closed by the same type of brackets.
		///Open brackets must be closed in the correct order.
		///Note that an empty string is also considered valid.</summary>
		public static bool IsValid(string s) {
			Stack<char> stackChars=new Stack<char>();
			for(int i = 0;i<s.Length;i++) {
				if(IsOpenBracket(s[i])) {
					stackChars.Push(s[i]);
				}
				else if(IsClosedBracket(s[i]) && stackChars.Count>0 && stackChars.Peek()==GetCorrespondingBracket(s[i])) {
					stackChars.Pop();
				}
				else {
					return false;
				}
			}
			return stackChars.Count==0;
		}

		private static char GetCorrespondingBracket(char input) {
			switch(input) {
				case '{':
					return '}';
				case '}':
					return '{';
				case '[':
					return ']';
				case ']':
					return '[';
				case '(':
					return ')';
				case ')':
					return '(';
				default://This should never happen
					return '\0';
			}
		}

		private static bool IsOpenBracket(char input) {
			if(input=='(' || input=='[' ||input=='{') {
				return true;
			}
			return false;
		}

		private static bool IsClosedBracket(char input) {
			if(input==')' || input==']' ||input=='}') {
				return true;
			}
			return false;
		}
		#endregion

		#region Medium Problems
		///<summary>You are given two non-empty linked lists representing two non-negative integers. 
		///The digits are stored in reverse order and each of their nodes contain a single digit. 
		///Add the two numbers and return it as a linked list. You may assume the two numbers do 
		///not contain any leading zero, except the number 0 itself.</summary>
		public static ListNode AddTwoNumbers(ListNode l1, ListNode l2) {
			int carry=0;
			ListNode l1Head=l1;
			ListNode l2Head=l2;
			ListNode retVal=new ListNode(0);
			ListNode curNode=retVal;
			while(l1Head!=null || l2Head!=null) {
				int l1Num=l1Head==null ? 0 : l1Head.val;
				int l2Num=l2Head==null ? 0 : l2Head.val;
				int sum=l1Num+l2Num+carry;
				carry=sum>=10 ? 1 : 0;
				curNode.next=new ListNode(sum%10);
				curNode=curNode.next;
				l1Head=l1Head?.next;
				l2Head=l2Head?.next;
			}
			if(carry>0) {
				curNode.next=new ListNode(carry);
			}
			return retVal.next;
		}

		///<summary>Given a ListNode, this method returns the integer value the linked list represents.
		///The parameter 'canReverse' will be set to false in unit tests. Per the problem definition
		///we will be given ListNodes in reverse order. For clarity's sake in unit tests, we will not do this
		///when testing that we have created the ListNode correctly.</summary>
		public static int GetListNodeNumber(ListNode listNode) {
			StringBuilder strBuild=new StringBuilder("");
			ListNode nodeCur=listNode;
			while(nodeCur!=null) {
				strBuild.Append(nodeCur.val);
				nodeCur=nodeCur.next;
			}
			char[] charArray=strBuild.ToString().ToCharArray();
			return Int32.Parse(new string(charArray));
		}

		///<summary>Creates a linked list of ListNode objects for the given integer value. Follows the definition of 
		///ListNode above.</summary>
		public static ListNode CreateListNode(int val) {
			char[] arrayChars=val.ToString().ToCharArray();
			Array.Reverse(arrayChars);
			ListNode lastNode;
			ListNode curNode=new ListNode(Int32.Parse(arrayChars[0].ToString()));
			ListNode retVal=curNode;//Preserve the very first node reference, this is our return value.
			for(int i = 1;i<arrayChars.Length;i++) {//Loop through starting at the 2nd char
				lastNode=curNode;
				curNode=new ListNode(Int32.Parse(arrayChars[i].ToString()));
				lastNode.next=curNode;
			}
			return retVal;
		}

		public class ListNode {
			public int val;
			public ListNode next;
			public ListNode(int x) { val = x; }
		}

		public static IList<IList<int>> ThreeSum(int[] nums) {
			IList<IList<int>> listNums=new List<IList<int>>();
			return listNums;
		}
		#endregion

		#region Hard Problems

		#endregion
	}
}
