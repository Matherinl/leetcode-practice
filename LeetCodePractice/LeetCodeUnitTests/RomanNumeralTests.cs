﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using LeetCodePractice;

namespace LeetCodeUnitTests {
	[TestClass]
	public class RomanNumeralTests {
		[TestMethod]
		public void RomanToInt_III_ShouldReturn3() {
			Assert.IsTrue(LeetCodeSolutions.RomanToInt("III")==3);
		}

		[TestMethod]
		public void RomanToInt_IV_ShouldReturn4() {
			Assert.IsTrue(LeetCodeSolutions.RomanToInt("IV")==4);
		}
		
		[TestMethod]
		public void RomanToInt_VIII_ShouldReturn8() {
			Assert.IsTrue(LeetCodeSolutions.RomanToInt("VIII")==8);
		}
		
		[TestMethod]
		public void RomanToInt_LVIII_ShouldReturn58() {
			Assert.IsTrue(LeetCodeSolutions.RomanToInt("LVIII")==58);
		}
		
		[TestMethod]
		public void RomanToInt_CM_ShouldReturn900() {
			Assert.IsTrue(LeetCodeSolutions.RomanToInt("CM")==900);
		}

		//Same unit tests but for the improved approach
		[TestMethod]
		public void RomanToIntImproved_III_ShouldReturn3() {
			Assert.IsTrue(LeetCodeSolutions.RomanToIntImproved("III")==3);
		}

		[TestMethod]
		public void RomanToIntImproved_IV_ShouldReturn4() {
			Assert.IsTrue(LeetCodeSolutions.RomanToIntImproved("IV")==4);
		}
		
		[TestMethod]
		public void RomanToIntImproved_VIII_ShouldReturn8() {
			Assert.IsTrue(LeetCodeSolutions.RomanToIntImproved("VIII")==8);
		}
		
		[TestMethod]
		public void RomanToIntImproved_LVIII_ShouldReturn58() {
			Assert.IsTrue(LeetCodeSolutions.RomanToIntImproved("LVIII")==58);
		}
		
		[TestMethod]
		public void RomanToIntImproved_CM_ShouldReturn900() {
			Assert.IsTrue(LeetCodeSolutions.RomanToIntImproved("CM")==900);
		}
	}
}
