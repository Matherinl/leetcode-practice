﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using LeetCodePractice;

namespace LeetCodeUnitTests {
	///<summary>Unit tests for LeetCode problem #9.</summary>
	[TestClass]
	public class PalindromeTests {
		[TestMethod]
		public void IsPalindrome_Negative111_ShouldReturnFalse() {
			Assert.IsFalse(LeetCodeSolutions.IsPalindrome(-111));
		}

		[TestMethod]
		public void IsPalindrome_1_ShouldReturnTrue() {
			Assert.IsTrue(LeetCodeSolutions.IsPalindrome(1));
		}

		[TestMethod]
		public void IsPalindrome_142000241_ShouldReturnTrue() {
			Assert.IsTrue(LeetCodeSolutions.IsPalindrome(142000241));
		}

		[TestMethod]
		public void IsPalindrome_90009_ShouldReturnTrue() {
			Assert.IsTrue(LeetCodeSolutions.IsPalindrome(90009));
		}

		[TestMethod]
		public void IsPalindrome_90409_ShouldReturnTrue() {
			Assert.IsTrue(LeetCodeSolutions.IsPalindrome(90409));
		}

		[TestMethod]
		public void IsPalindrome_121111211_ShouldReturnFalse() {
			Assert.IsFalse(LeetCodeSolutions.IsPalindrome(121111211));
		}
		[TestMethod]
		public void IsPalindromeEnhanced_Negative111_ShouldReturnFalse() {
			Assert.IsFalse(LeetCodeSolutions.IsPalindromeEnhanced(-111));
		}

		[TestMethod]
		public void IsPalindromeEnhanced_11_ShouldReturnTrue() {
			Assert.IsTrue(LeetCodeSolutions.IsPalindromeEnhanced(11));
		}

		[TestMethod]
		public void IsPalindromeEnhanced_142000241_ShouldReturnTrue() {
			Assert.IsTrue(LeetCodeSolutions.IsPalindromeEnhanced(142000241));
		}

		[TestMethod]
		public void IsPalindromeEnhanced_90009_ShouldReturnTrue() {
			Assert.IsTrue(LeetCodeSolutions.IsPalindromeEnhanced(90009));
		}

		[TestMethod]
		public void IsPalindromeEnhanced_90409_ShouldReturnTrue() {
			Assert.IsTrue(LeetCodeSolutions.IsPalindromeEnhanced(90409));
		}

		[TestMethod]
		public void IsPalindromeEnhanced_121111211_ShouldReturnFalse() {
			Assert.IsFalse(LeetCodeSolutions.IsPalindromeEnhanced(121111211));
		}
	}
}
