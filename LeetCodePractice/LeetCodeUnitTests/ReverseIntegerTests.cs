using Microsoft.VisualStudio.TestTools.UnitTesting;
using LeetCodePractice;
namespace LeetCodeUnitTests {
	///<summary>Unit tests for LeetCode problem #7.</summary>
	[TestClass]
	public class ReverseIntegerTests {
		[TestMethod]
		public void ReverseInteger_123_ShouldReturn321() {
			Assert.AreEqual(LeetCodeSolutions.Reverse(123),321);
		}

		[TestMethod]
		public void ReverseInteger_Negative123_ShouldReturnNegative321() {
			Assert.AreEqual(LeetCodeSolutions.Reverse(-123),-321);
		}

		[TestMethod]
		public void ReverseInteger_120_ShouldReturn21() {
			Assert.AreEqual(LeetCodeSolutions.Reverse(120),21);
		}

		[TestMethod]
		public void ReverseInteger_002100_ShouldReturn1200() {
			Assert.AreEqual(LeetCodeSolutions.Reverse(2100),12);
		}

		[TestMethod]
		public void ReverseInteger_0_ShouldReturn0() {
			Assert.AreEqual(LeetCodeSolutions.Reverse(0),0);
		}
	}
}
