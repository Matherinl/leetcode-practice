﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using static LeetCodePractice.LeetCodeSolutions;

namespace LeetCodeUnitTests {
	[TestClass]
	public class LongestPrefixTests {
		[TestMethod]
		public void GetLongestPrefix_flowers_flow_flight_ShouldReturn_fl() {
			string[] strs=new string[]{"flower","flow","flight"};
			Assert.AreEqual("fl",LongestCommonPrefix(strs));
		}

		[TestMethod]
		public void GetLongestPrefix_dog_racecar_car_ShouldReturn_Blank() {
			string[] strs=new string[]{"dog","racecar","car"};
			Assert.AreEqual("",LongestCommonPrefix(strs));
		}

		[TestMethod]
		public void GetLongestPrefix_aaaaaaaaaaaa_aaaaaaaaaaab_aaaaaaaaaaac_flow_flight_ShouldReturn_fl() {
			string[] strs=new string[]{"aaaaaaaaaaaa","aaaaaaaaaaab","aaaaaaaaaaac"};
			Assert.AreEqual("aaaaaaaaaaa",LongestCommonPrefix(strs));
		}

		[TestMethod]
		public void GetLongestPrefix_blank_ShouldReturn_blank() {
			string[] strs=new string[]{""};
			Assert.AreEqual("",LongestCommonPrefix(strs));
		}

		[TestMethod]
		public void GetLongestPrefix_a_ShouldReturn_a() {
			string[] strs=new string[]{"a"};
			Assert.AreEqual("a",LongestCommonPrefix(strs));
		}

		[TestMethod]
		public void GetLongestPrefix_blank_blank_ShouldReturn_blank() {
			string[] strs=new string[]{"",""};
			Assert.AreEqual("",LongestCommonPrefix(strs));
		}

		[TestMethod]
		public void GetLongestPrefix_b_blank_blank_ShouldReturn_blank() {
			string[] strs=new string[]{"b","",""};
			Assert.AreEqual("",LongestCommonPrefix(strs));
		}

		[TestMethod]
		public void GetLongestPrefix_b_blank_ShouldReturn_blank() {
			string[] strs=new string[]{"b",""};
			Assert.AreEqual("",LongestCommonPrefix(strs));
		}

		[TestMethod]
		public void GetLongestPrefix_c_c_ShouldReturn_c() {
			string[] strs=new string[]{"c","c"};
			Assert.AreEqual("c",LongestCommonPrefix(strs));
		}

		[TestMethod]
		public void GetLongestPrefix_aca_cba_ShouldReturn_blank() {
			string[] strs=new string[]{"aca","cba"};
			Assert.AreEqual("",LongestCommonPrefix(strs));
		}
	}
}
