﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using static LeetCodePractice.LeetCodeSolutions;

namespace LeetCodeUnitTests {
	[TestClass]
	public class AddTwoNumbersTests {		
		[TestMethod]
		public void GetListNodeNumber_342_465_ReturnsListNodeValOf708() {
			ListNode listNode1=CreateListNode(342);
			ListNode listNode2=CreateListNode(465);
			ListNode retVal=AddTwoNumbers(listNode1,listNode2);
			Assert.AreEqual(708,GetListNodeNumber(retVal));
		}
	}
}
