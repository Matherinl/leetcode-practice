﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using LeetCodePractice;

namespace LeetCodeUnitTests {
	///<summary>The test names are difficult because our test cases are built from reserved characters in C#.</summary>
	[TestClass]
	public class ValidParanthesesTests {
		[TestMethod]
		public void IsValid_1_ShouldReturnTrue() {
			Assert.IsTrue(LeetCodeSolutions.IsValid("()"));
		}
		
		[TestMethod]
		public void IsValid_2_ShouldReturnTrue() {
			Assert.IsTrue(LeetCodeSolutions.IsValid("()[]{}"));
		}

		[TestMethod]
		public void IsValid_3_ShouldReturnFalse() {
			Assert.IsFalse(LeetCodeSolutions.IsValid("(]"));
		}

		[TestMethod]
		public void IsValid_4_ShouldReturnFalse() {
			Assert.IsFalse(LeetCodeSolutions.IsValid("([)]"));
		}

		[TestMethod]
		public void IsValid_5_ShouldReturnTrue() {
			Assert.IsTrue(LeetCodeSolutions.IsValid("{[]}"));
		}

		[TestMethod]
		public void IsValid_6_ShouldReturnTrue() {
			Assert.IsTrue(LeetCodeSolutions.IsValid("(([]){})"));
		}
	}
}
